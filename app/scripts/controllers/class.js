'use strict';

/**
 * @ngdoc function
 * @name masterclassApp.controller:ClassCtrl
 * @description
 * # ClassCtrl
 * Controller of the masterclassApp
 */
angular.module('masterclassApp')
  .controller('ClassCtrl', function ($stateParams, $rootScope, $scope, _, masterclassService, userService, $modal) {

    $scope.$on('$viewContentLoaded', function() {
      $("#example-basic").treetable({ expandable: true });
    });

    $scope.initialize = function() {

      setTimeout(function(){
        var mapOptions = {
          zoom: 8,
          center: new google.maps.LatLng(-34.397, 150.644)
        };

        var marker;

        var map = new google.maps.Map(document.getElementById('map-canvas'),
              mapOptions);

        google.maps.event.addListener(map, 'click', function(event) {
            placeMarker(event.latLng);
        });

        function placeMarker(location) {

            if (marker == undefined){
                marker = new google.maps.Marker({
                    position: location,
                    map: map,
                    animation: google.maps.Animation.DROP,
                });
            }
            else{
                marker.setMap(null)

                marker = new google.maps.Marker({
                    position: location,
                    map: map,
                    animation: google.maps.Animation.DROP,
                });
            }

        }

      }, 2000)
      
    }

    $scope.stores = [
      {id: 1, name: 'Sydney Discovery'},
      {id: 2, name: 'World Square'},
      {id: 3, name: 'Elizabeth Square'}
    ]

    $scope.isLoggedIn = userService.isLogin();

    $rootScope.$on('login', function(event, mass) {
      $scope.isLoggedIn = userService.isLogin();
    });

    $rootScope.$on('logout', function(event, mass) {
      $scope.isLoggedIn = userService.isLogin();
    });

    var scheduleClassModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/scheduleClassModal.html',
      show: false
    });

    $scope.scheduleClassModal = function(){
       scheduleClassModal.$promise.then(function(){
        scheduleClassModal.show();
        $scope.initialize();
      });
    }

    var scheduleClassConfirmationModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/scheduleClassConfirmationModal.html',
      show: false
    });

    $scope.scheduleClassConfirmationModal = function(){
       scheduleClassConfirmationModal.$promise.then(scheduleClassConfirmationModal.show);
    }

    $scope.participants = [
      {name: "Vincent Vega", phone: "0404 456 123", email: "vincentv@bfm.com.au"},
      {name: "Julles Winnfield", phone: "0404 456 123", email: "jullesw@bfm.com.au"},
      {name: "Mia Wallace", phone: "0404 456 123", email: "miaw@bfm.com.au"},
      {name: "Butch Coolidge", phone: "0404 456 123", email: "butchc@bfm.com.au"},
      {name: "Winston Wolfe", phone: "0404 456 123", email: "winstonw@bfm.com.au"},
      {name: "Marsellus Wallace", phone: "0404 456 123", email: "marcellusw@bfm.com.au"}
    ];

    $scope.masterclass = angular.copy(masterclassService.getMasterclass($stateParams.classId));

    $scope.scheduled = [
      {id: 1, label: 'Today', selected: false},
      {id: 2, label: 'This Week', selected: false},
      {id: 3, label: 'Next Week', selected: false},
      {id: 4, label: 'This Month', selected: true}
    ]

    $scope.setScheduled = function(scheduledid) {
      _.each(
        $scope.scheduled,
        function(schedule){
          if(scheduledid == schedule.id){
            schedule.selected = true;
          } else {
            schedule.selected = false;
          }
        }
      );
    }

    $scope.back = function(){
      window.history.back();
    }

  });
