'use strict';

/**
 * @ngdoc function
 * @name masterclassApp.controller:UserCtrl
 * @description
 * # UserCtrl
 * Controller of the masterclassApp
 */
angular.module('masterclassApp')
  .controller('UserCtrl', function ( $rootScope, $scope, userService, $modal, $timeout ) {

    $scope.isLoggedIn = userService.isLogin();

    var loadingModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/loadingModal.html',
      show: false
    });

    // loadingModal.$promise.then(loadingModal.show);

    $scope.loadingModal = function() {
      loadingModal.$promise.then(loadingModal.show);
    };

    $scope.login = function(){

      $scope.message = "Logging In";
      loadingModal.$promise.then(loadingModal.show);

      $timeout(function(){
        loadingModal.hide();
        $scope.isLoggedIn = userService.login();
        $rootScope.$emit('login');
      }, 1000);

    }

    $scope.logout = function(){
      
      $scope.message = "Logging Out";
      loadingModal.$promise.then(loadingModal.show);

      $timeout(function(){
        loadingModal.hide();
        $scope.isLoggedIn = userService.logout();
        $rootScope.$emit('logout');
      }, 1000);
    }
    
  });


angular.module('masterclassApp')
  .controller('LoginCtrl', function ( $rootScope, $scope, userService, $modal, $timeout ) {

    $scope.isLoggedIn = userService.isLogin();

    var loadingModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/loadingModal.html',
      show: false
    });

    $scope.login = function(){

      $scope.message = "Logging In";
      loadingModal.$promise.then(loadingModal.show);

      $timeout(function(){
        loadingModal.hide();
        $scope.isLoggedIn = userService.login();
        $rootScope.$emit('login');
      }, 1000);

    }

    $scope.logout = function(){
      
      $scope.message = "Logging Out";
      loadingModal.$promise.then(loadingModal.show);

      $timeout(function(){
        loadingModal.hide();
        $scope.isLoggedIn = userService.logout();
        $rootScope.$emit('logout');
      }, 1000);
    }
    
  });