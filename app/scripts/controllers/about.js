'use strict';

/**
 * @ngdoc function
 * @name masterclassApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the masterclassApp
 */
angular.module('masterclassApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
