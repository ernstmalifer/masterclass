'use strict';

/**
 * @ngdoc function
 * @name masterclassApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the masterclassApp
 */
angular.module('masterclassApp')
  .controller('MainCtrl', function ($scope, _, masterclassService) {

    $scope.$on('$viewContentLoaded', function() {

    });

    $scope.storeid = 1;
    $scope.storename = 'Sydney Discover';

    $scope.masterclasses = angular.copy(masterclassService.getMasterclasses());
    _.each($scope.masterclasses, function(masterclass){
      masterclass.schedulesfor = _.find(masterclass.schedules, function(schedule){ return schedule.store == $scope.storeid });
    })

    $scope.stores = [
      {id: 1, name: 'Sydney Discovery'},
      {id: 2, name: 'World Square'},
      {id: 3, name: 'Elizabeth Square'}
    ]

    $scope.classstatuses = [
      {id: 1, label: 'What\'s in my store', checked: true},
      {id: 2, label: 'Active', checked: false},
      {id: 3, label: 'Inactive', checked: false},
    ]

    $scope.scheduled = [
      {id: 1, label: 'Today', selected: false},
      {id: 2, label: 'This Week', selected: false},
      {id: 3, label: 'Next Week', selected: false},
      {id: 4, label: 'This Month', selected: true}
    ]

    $scope.setScheduled = function(scheduledid) {
      _.each(
        $scope.scheduled,
        function(schedule){
          if(scheduledid == schedule.id){
            schedule.selected = true;
          } else {
            schedule.selected = false;
          }
        }
      );
    }

    $scope.categories = [
      {id: 1, label: 'Essentials', selected: true},
      {id: 2, label: 'Knowledge Builders', selected: true},
      {id: 3, label: 'Events', selected: true}
    ]

    $scope.setCategory = function(categoryid) {
      _.each(
        $scope.categories,
        function(category){
          if(categoryid == category.id){
            category.selected = category.selected ? false : true;
          }
        }
      );
    }

    $scope.tags = [
      {id: 1, label: 'iPhone', selected: true},
      {id: 2, label: 'Android', selected: true},
      {id: 3, label: 'Apps', selected: true},
      {id: 4, label: 'Broadband', selected: true},
      {id: 5, label: 'Security', selected: true},
      {id: 6, label: 'Cloud', selected: true}
    ]

    $scope.setTag = function(tagid) {
      _.each(
        $scope.tags,
        function(tag){
          if(tagid == tag.id){
            tag.selected = tag.selected ? false : true;
          }
        }
      );
    }    

    $scope.filterStore = function(){
      
    }

    $scope.filterActive = function(masterclass){

      // console.log(masterclass)

      var flag = false;
      if($scope.classstatuses[0].checked){
        var masterclassStores = _.pluck(masterclass.schedules, 'store');

        // console.log(masterclass.schedules)

        if(_.contains(masterclassStores, $scope.storeid)) {
          flag = true;
        }
      }

      // if active
      if($scope.classstatuses[1].checked){
        if(masterclass.isactive) {
          flag = true;
        }
      }

      // if inactive
      if($scope.classstatuses[2].checked){
        if(!masterclass.isactive) {
          flag = true;
        }
      }

      return flag;
    }

    $scope.filterSchedule = function(masterclass){
      var flag = false;

      var selectedScheduledID = _.pluck(_.filter($scope.scheduled, function(schedule){ return schedule.selected; }), 'id');
      var masterclassScheduledIds = masterclass.scheduledfor;

      _.each(masterclassScheduledIds, function(masterclassScheduledId){
        if( _.contains(selectedScheduledID, masterclassScheduledId) ){
          flag = true;
        }
      });

      return flag;
    }

    $scope.filterCategory = function(masterclass){

      var flag = false;

      var selectedCategoryIDs = _.pluck(_.filter($scope.categories, function(category){ return category.selected; }), 'id');
      var masterclassCategoryID = masterclass.category.id

      if( _.contains(selectedCategoryIDs, masterclassCategoryID) ){
        flag = true;
      }

      return flag;

    }

    $scope.filterTags = function(masterclass){

      var flag = false;

      var selectedTagIDs = _.pluck(_.filter($scope.tags, function(tag){ return tag.selected; }), 'id');
      var masterclassTagIDs = _.pluck(masterclass.tags, 'id')

      _.each(selectedTagIDs, function(selectedTagID){
        if( _.contains(masterclassTagIDs, selectedTagID) ){
          flag = true;
        }
      })

      return flag;
      
    }

  });
