'use strict';

/**
 * @ngdoc function
 * @name masterclassApp.controller:ClassCtrl
 * @description
 * # ClassCtrl
 * Controller of the masterclassApp
 */
angular.module('masterclassApp')
  .controller('ClassScheduleCtrl', function ($stateParams, $rootScope, $scope, _, masterclassService, userService, $modal) {
      
    $scope.initialize = function() {

      setTimeout(function(){
        var mapOptions = {
          zoom: 8,
          center: new google.maps.LatLng(-34.397, 150.644)
        };

        var marker;

        var map = new google.maps.Map(document.getElementById('map-canvas'),
              mapOptions);

        google.maps.event.addListener(map, 'click', function(event) {
            placeMarker(event.latLng);
        });

        function placeMarker(location) {

            if (marker == undefined){
                marker = new google.maps.Marker({
                    position: location,
                    map: map,
                    animation: google.maps.Animation.DROP,
                });
            }
            else{
                marker.setMap(null)

                marker = new google.maps.Marker({
                    position: location,
                    map: map,
                    animation: google.maps.Animation.DROP,
                });
            }

        }

      }, 2000)
      
    }

    $scope.$on('$viewContentLoaded', function() {
      $("#example-basic").treetable({ expandable: true });
    });

    $scope.stores = [
      {id: 1, name: 'Sydney Discovery'},
      {id: 2, name: 'World Square'},
      {id: 3, name: 'Elizabeth Square'}
    ]

    $scope.isLoggedIn = userService.isLogin();

    $rootScope.$on('login', function(event, mass) {
      $scope.isLoggedIn = userService.isLogin();
    });

    $rootScope.$on('logout', function(event, mass) {
      $scope.isLoggedIn = userService.isLogin();
    });

    var changeDateModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/changeDateModal.html',
      show: false
    });

    var changeLocationModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/changeLocationModal.html',
      show: false
    });

    var addParticipantModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/addParticipantModal.html',
      show: false
    });

    var scheduleClassModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/editScheduleClassModal.html',
      show: false
    });

    var editSupportModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/editSupportModal.html',
      show: false
    });

    var addMaterialModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/addMaterialModal.html',
      show: false
    });

    var editParticipantModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/editParticipantModal.html',
      show: false
    });

    // addMaterialModal.$promise.then(addMaterialModal.show);

    $scope.changeDateModal = function() {
      changeDateModal.$promise.then(changeDateModal.show);
    };

    $scope.changeLocationModal = function() {
      changeLocationModal.$promise.then(changeLocationModal.show);
    };

    $scope.showAddParticipantModal = function() {
      addParticipantModal.$promise.then(addParticipantModal.show);
    };

    $scope.scheduleClassModal = function(){
       scheduleClassModal.$promise.then(function(){
        scheduleClassModal.show()
        $scope.initialize();
      });
    }

    $scope.$on('modal-shown', function() {
      $scope.initialize();
    });

    $scope.editSupportModal = function(){
       editSupportModal.$promise.then(editSupportModal.show);
    }

    $scope.addMaterialModal = function(){
       addMaterialModal.$promise.then(addMaterialModal.show);
    }

    $scope.participant = {};

    $scope.showEditParticipantModal = function(participant){
      $scope.participant = participant;
      editParticipantModal.$promise.then(editParticipantModal.show);
    }

    $scope.participants = [
      {name: "Vincent Vega", phone: "0404 456 123", email: "vincentv@bfm.com.au"},
      {name: "Julles Winnfield", phone: "0404 456 123", email: "jullesw@bfm.com.au"},
      {name: "Mia Wallace", phone: "0404 456 123", email: "miaw@bfm.com.au"},
      {name: "Butch Coolidge", phone: "0404 456 123", email: "butchc@bfm.com.au"},
      {name: "Winston Wolfe", phone: "0404 456 123", email: "winstonw@bfm.com.au"},
      {name: "Marsellus Wallace", phone: "0404 456 123", email: "marcellusw@bfm.com.au"}
    ];

    $scope.masterclass = angular.copy(masterclassService.getMasterclass($stateParams.classId));
    $scope.scheduleId = $stateParams.scheduleId;

    $scope.scheduled = [
      {id: 1, label: 'Today', selected: false},
      {id: 2, label: 'This Week', selected: false},
      {id: 3, label: 'Next Week', selected: false},
      {id: 4, label: 'This Month', selected: true}
    ]

    $scope.setScheduled = function(scheduledid) {
      _.each(
        $scope.scheduled,
        function(schedule){
          if(scheduledid == schedule.id){
            schedule.selected = true;
          } else {
            schedule.selected = false;
          }
        }
      );
    }

    var addParticipantAgainModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/addParticipantAgainModal.html',
      show: false
    });

    $scope.showConfirmation = function(){
      addParticipantAgainModal.$promise.then(addParticipantAgainModal.show);
    }

    var editParticipantAgainModal = $modal({
      animation: 'am-fade-and-slide-top',
      scope: $scope,
      template: 'views/editParticipantAgainModal.html',
      show: false
    });

    $scope.showEditConfirmation = function(){
      editParticipantAgainModal.$promise.then(editParticipantAgainModal.show);
    }

    $scope.back = function(){
      window.location.hash = '#/';
    }

  });
