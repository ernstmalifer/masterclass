'use strict';

/**
 * @ngdoc service
 * @name masterclassApp.userService
 * @description
 * # userService
 * Service in the masterclassApp.
 */
angular.module('masterclassApp')
  .service('userService', function () {
    // AngularJS will instantiate a singleton by calling "new" on this function

    this.loggedin = true;

    this.isLogin = function(){
      return this.loggedin;
    }

    this.login = function(){
      this.loggedin = true;
      return this.loggedin;
    }

    this.logout = function(){
      this.loggedin = false;
      return this.loggedin;
    }

  });
