'use strict';

/**
 * @ngdoc service
 * @name masterclassApp.masterclassService
 * @description
 * # masterclassService
 * Service in the masterclassApp.
 */
angular.module('masterclassApp')
  .service('masterclassService', function (_) {
    // AngularJS will instantiate a singleton by calling 'new' on this function

    this.masterclasses = [
        { id: 234, scheduledfor: [1,4], isnew: true, isactive: true, title: 'Fitter, Faster & Healthier', category: {id: 2, label: 'Knowledge Builders'}, description: 'Learn how you can use your technology to track and improve your health and fitness using apps, devices and wearable technology', location: {id: 1, name: 'Sydney Discovery', address: '400 George Street, Sydney NSW 2000'}, tags: [{id: 1, label: 'iPhone'}, {id: 2, label: 'Android'}],
          schedules: [
              { 
                store: 1, label: 'Sydney Discovery',
                schedules: [
                  {id: 1, label: '10:00am Mon 15 Aug'},
                  {id: 2, label: '10:00am Mon 15 Aug'}
                ]
              },
              { 
                store: 2, label: 'World Square',
                schedules: [
                  {id: 5, label: '10:00am Mon 14 Aug'},
                  {id: 6, label: '10:00am Mon 14 Aug'},
                  {id: 7, label: '10:00am Mon 14 Aug'},
                ]
              },
              { 
                store: 3, label: 'Elizabeth Square',
                schedules: [
                  {id: 8, label: '10:00am Mon 16 Aug'},
                  {id: 9, label: '10:00am Mon 16 Aug'},
                  {id: 10, label: '10:00am Mon 16 Aug'},
                ]
              }
            ]
        },
        
        { id: 142, scheduledfor: [2,4], isnew: false, isactive: true, title: 'Calling and Messaging Online', category: {id: 2, label: 'Knowledge Builders'}, description: 'An overview of the different ways you can communicate with friends & family no matter where they are over the internet', location: {id: 1, name: 'Sydney Discovery', address: '400 George Street, Sydney NSW 2000'}, tags: [{id: 4, label: 'Broadband'}, {id: 5, label: 'Security'}, {id: 6, label: 'Cloud'}],
        schedules: [
              { 
                store: 1, label: 'Sydney Discovery',
                schedules: [
                  {id: 1, label: '10:00am Mon 15 Aug'}
                ]
              },
              { 
                store: 2, label: 'World Square',
                schedules: [
                  {id: 5, label: '10:00am Mon 14 Aug'},
                  {id: 6, label: '10:00am Mon 17 Aug'},
                  {id: 7, label: '10:00am Mon 14 Aug'},
                ]
              },
              { 
                store: 3, label: 'Elizabeth Square',
                schedules: [
                  {id: 8, label: '10:00am Mon 16 Aug'},
                  {id: 9, label: '10:00am Mon 16 Aug'},
                  {id: 10, label: '10:00am Mon 16 Aug'},
                ]
              }
            ]
        },
        
        { id: 628, scheduledfor: [3,4], isnew: false, isactive: true, title: 'Social Networking Basics', category: {id: 2, label: 'Knowledge Builders'}, description: 'Create and maintain your online social presence, share stuff with friends and family and stay in touch', location: {id: 1, name: 'Sydney Discovery', address: '400 George Street, Sydney NSW 2000'}, tags: [{id: 1, label: 'iPhone'}, {id: 3, label: 'Apps'}],  
           schedules: [
              { 
                store: 1, label: 'Sydney Discovery',
                schedules: [
                  {id: 1, label: '10:00am Mon 15 Aug'},
                  {id: 2, label: '10:00am Mon 15 Aug'}
                ]
              },
              { 
                store: 2, label: 'World Square',
                schedules: [
                  {id: 5, label: '10:00am Mon 14 Aug'},
                  {id: 6, label: '10:00am Mon 17 Aug'},
                  {id: 7, label: '10:00am Mon 14 Aug'},
                ]
              },
              { 
                store: 3, label: 'Elizabeth Square',
                schedules: [
                  {id: 8, label: '10:00am Mon 16 Aug'},
                  {id: 9, label: '10:00am Mon 16 Aug'},
                  {id: 10, label: '10:00am Mon 16 Aug'},
                ]
              }
            ]
        },
        
        { id: 289, scheduledfor: [2,4], isnew: false, isactive: true, title: 'Health & Fitness', category: {id: 2, label: 'Knowledge Builders'}, description: 'Learn how you can use your technology to track and improve your health and fitness using apps, devices and wearable technology', location: {id: 1, name: 'Sydney Discovery', address: '400 George Street, Sydney NSW 2000'}, tags: [{id: 3, label: 'Apps'}],
           schedules: [
              { 
                store: 1, label: 'Sydney Discovery',
                schedules: [
                  {id: 1, label: '10:00am Mon 15 Aug'},
                  {id: 2, label: '10:00am Mon 15 Aug'},
                  {id: 3, label: '10:00am Mon 15 Aug'}
                ]
              },
              { 
                store: 2, label: 'World Square',
                schedules: [
                  {id: 5, label: '10:00am Mon 14 Aug'},
                  {id: 6, label: '10:00am Mon 17 Aug'},
                  {id: 7, label: '10:00am Mon 14 Aug'},
                ]
              },
              { 
                store: 3, label: 'Elizabeth Square',
                schedules: [
                  {id: 8, label: '10:00am Mon 16 Aug'},
                  {id: 9, label: '10:00am Mon 16 Aug'},
                  {id: 10, label: '10:00am Mon 16 Aug'},
                ]
              }
            ]
        },

        { id: 342, scheduledfor: [1,4], isnew: false, isactive: true, title: 'Connected Home: Your Home, Your Way', category: {id: 1, label: 'Essentials'}, description: 'Imagine a home where technology just works, so you’re free to enjoy the entertainment, voice and internet experience you want. It’s your home, your way...', location: {id: 1, name: 'Sydney Discovery', address: '400 George Street, Sydney NSW 2000'}, tags: [{id: 4, label: 'Broadband'}],
         schedules: [
              { 
                store: 1, label: 'Sydney Discovery',
                schedules: [
                  {id: 1, label: '10:00am Mon 15 Aug'}
                ]
              },
              { 
                store: 2, label: 'World Square',
                schedules: [
                  {id: 5, label: '10:00am Mon 14 Aug'},
                  {id: 6, label: '10:00am Mon 17 Aug'},
                  {id: 7, label: '10:00am Mon 14 Aug'},
                ]
              },
              { 
                store: 3, label: 'Elizabeth Square',
                schedules: [
                  {id: 8, label: '10:00am Mon 16 Aug'},
                  {id: 9, label: '10:00am Mon 16 Aug'},
                  {id: 10, label: '10:00am Mon 16 Aug'},
                ]
              }
            ]
        },

        { id: 566, scheduledfor: [1,4], title: 'More surfing, less hassle when using data on your mobile', category: {id: 1, label: 'Essentials'}, description: 'Now, the mobile web is as important to our daily smartphone experience as voice and text. It’s so vital that many of you tell us that from time to time you find yourselves using more data in a month than your plan allowance caters for.', location: {id: 1, name: 'Sydney Discovery', address: '400 George Street, Sydney NSW 2000'}, tags: [{id: 1, label: 'iPhone'},{id: 2, label: 'Android'},{id: 3, label: 'Apps'}], 
         schedules: [
              { 
                store: 1, label: 'Sydney Discovery',
                schedules: [
                  {id: 1, label: '10:00am Mon 15 Aug'},
                  {id: 2, label: '10:00am Mon 15 Aug'}
                ]
              },
              { 
                store: 2, label: 'World Square',
                schedules: [
                  {id: 5, label: '10:00am Mon 14 Aug'},
                  {id: 6, label: '10:00am Mon 17 Aug'},
                  {id: 7, label: '10:00am Mon 14 Aug'},
                ]
              },
              { 
                store: 3, label: 'Elizabeth Square',
                schedules: [
                  {id: 8, label: '10:00am Mon 16 Aug'},
                  {id: 9, label: '10:00am Mon 16 Aug'},
                  {id: 10, label: '10:00am Mon 16 Aug'},
                ]
              }
            ]
        },

        { id: 123, scheduledfor: [1,4], title: 'Samsung Galaxy S6 and Galaxy S6 edge to launch with Telstra', category: {id: 3, label: 'Events'}, description: 'Telstra customers can look forward to more data, more entertainment and more speed in 4GX areas, when the Samsung Galaxy S6 and S6 edge launch with Telstra next Friday 10 April.', location: {id: 1, name: 'Sydney Discovery', address: '400 George Street, Sydney NSW 2000'}, tags: [{id: 2, label: 'Android'}], 
         schedules: [
              { 
                store: 1, label: 'Sydney Discovery',
                schedules: [
                  {id: 1, label: '10:00am Mon 15 Aug'},
                  {id: 2, label: '10:00am Mon 15 Aug'},
                  {id: 3, label: '10:00am Mon 15 Aug'},
                  {id: 4, label: '10:00am Mon 15 Aug'}
                ]
              },
              { 
                store: 2, label: 'World Square',
                schedules: [
                  {id: 5, label: '10:00am Mon 14 Aug'},
                  {id: 6, label: '10:00am Mon 17 Aug'},
                  {id: 7, label: '10:00am Mon 14 Aug'},
                ]
              },
              { 
                store: 3, label: 'Elizabeth Square',
                schedules: [
                  {id: 8, label: '10:00am Mon 16 Aug'},
                  {id: 9, label: '10:00am Mon 16 Aug'},
                  {id: 10, label: '10:00am Mon 16 Aug'},
                ]
              }
            ]
        },

        { id: 712, scheduledfor: [2,4], title: 'Addressing the digital divide', category: {id: 2, label: 'Knowledge Builders'}, description: 'Digital connectivity is increasingly seen as an essential service, with access to the internet in many parts of the world now underpinning economic development, social connections, education, the arts, employment and social services.', location: {id: 1, name: 'Sydney Discovery', address: '400 George Street, Sydney NSW 2000'}, tags: [{id: 1, label: 'iPhone'},{id: 2, label: 'Android'},{id: 3, label: 'Apps'},{id: 4, label: 'Broadband'}], 
         schedules: [
              { 
                store: 1, label: 'Sydney Discovery',
                schedules: [
                  {id: 1, label: '10:00am Mon 15 Aug'},
                  {id: 2, label: '10:00am Mon 15 Aug'},
                ]
              },
              { 
                store: 2, label: 'World Square',
                schedules: [
                  {id: 5, label: '10:00am Mon 14 Aug'},
                  {id: 6, label: '10:00am Mon 17 Aug'},
                  {id: 7, label: '10:00am Mon 14 Aug'},
                ]
              },
              { 
                store: 3, label: 'Elizabeth Square',
                schedules: [
                  {id: 8, label: '10:00am Mon 16 Aug'},
                  {id: 9, label: '10:00am Mon 16 Aug'},
                  {id: 10, label: '10:00am Mon 16 Aug'},
                ]
              }
            ]
        },

        { id: 489, scheduledfor: [3,4], title: 'Securing your data', category: {id: 2, label: 'Knowledge Builders'}, description: 'The Australian Government has now passed legislation to create a data retention scheme. Under the scheme, all telcos will be required to collect and store a significant amount of customer metadata for two years and make it available upon lawful request to certain law enforcement and national security agencies.', location: {id: 1, name: 'Sydney Discovery', address: '400 George Street, Sydney NSW 2000'}, tags: [{id: 5, label: 'Security'}, {id: 6, label: 'Cloud'}], 
         schedules: [
              { 
                store: 1, label: 'Sydney Discovery',
                schedules: [
                  {id: 1, label: '10:00am Mon 15 Aug'}
                ]
              },
              { 
                store: 2, label: 'World Square',
                schedules: [
                  {id: 5, label: '10:00am Mon 14 Aug'},
                  {id: 6, label: '10:00am Mon 17 Aug'},
                  {id: 7, label: '10:00am Mon 14 Aug'},
                ]
              },
              { 
                store: 3, label: 'Elizabeth Square',
                schedules: [
                  {id: 8, label: '10:00am Mon 16 Aug'},
                  {id: 9, label: '10:00am Mon 16 Aug'},
                  {id: 10, label: '10:00am Mon 16 Aug'},
                ]
              }
            ]
        },
      ];

    this.getMasterclasses = function(){
      return this.masterclasses;
    }

    this.getMasterclass = function(id){
      return _.find(this.masterclasses, function(masterclass){ return masterclass.id == id });
    }

  });
