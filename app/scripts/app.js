'use strict';

/**
 * @ngdoc overview
 * @name masterclassApp
 * @description
 * # masterclassApp
 *
 * Main module of the application.
 */
angular
  .module('masterclassApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    // 'ngRoute', //replaced by ui.router
    'ui.router',
    'ngSanitize',
    'ngTouch',
    'mgcrea.ngStrap'
  ])
  .config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');

    $stateProvider
    .state('main', {
      url: '/',
      views: {
        'whitemaincontainer': {
          templateUrl: 'views/main.html',
          controller: 'MainCtrl'
        },
        'header': {
          templateUrl: 'views/header.html',
          controller: 'UserCtrl'
        }
      }
    })
    .state('class', {
      url: '/class/:classId',
      views: {
        'whitemaincontainer': {
          templateUrl: 'views/class.html',
          controller: 'ClassCtrl'
        },
        'header': {
          templateUrl: 'views/header.html',
          controller: 'UserCtrl'
        }
      }
    })
    .state('classschedule', {
      url: '/class/:classId/:scheduleId',
      views: {
        'whitemaincontainer': {
          templateUrl: 'views/classschedule.html',
          controller: 'ClassScheduleCtrl'
        },
        'header': {
          templateUrl: 'views/header.html',
          controller: 'UserCtrl'
        }
      }
    });

  });