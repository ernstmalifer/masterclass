'use strict';

describe('Service: masterclassService', function () {

  // load the service's module
  beforeEach(module('masterclassApp'));

  // instantiate service
  var masterclassService;
  beforeEach(inject(function (_masterclassService_) {
    masterclassService = _masterclassService_;
  }));

  it('should do something', function () {
    expect(!!masterclassService).toBe(true);
  });

});
